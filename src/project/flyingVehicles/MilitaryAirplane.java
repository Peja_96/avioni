package project.flyingVehicles;

import project.simulation.Simulator;

import static project.simulation.Simulator.sizeX;
import static project.simulation.Simulator.sizeY;

public  class MilitaryAirplane extends Airplane {
    private Object[] equipment;
    public static Object lock = new Object();
    private boolean enemy;
    public MilitaryAirplane(String color,int x, int y,boolean enemy){
        super("A",color,x,y); this.enemy = enemy;
    }

    public boolean getEnemy(){return enemy;}

    public void run(){
        while(true){
            if(enemy)
                super.run();
            else {
                if (direction == Direction.DOWN)
                    ++coordinateX;
                else if (direction == Direction.UP)
                    --coordinateX;
                else if (direction == Direction.RIGHT)
                    ++coordinateY;
                else
                    --coordinateY;
                if (!(coordinateX < sizeX && coordinateY < sizeY && coordinateX >= 0 && coordinateY >= 0)) {
                    Simulator.list.remove(this);
                    break;
                }
                attack();
                try {
                    sleep(1000 * speed);
                } catch (InterruptedException ex) {
                }
            }
        }
    }
    public  void attack(){
        synchronized (lock) {
            for (FlyingVehicle f : Simulator.listCopy())
                if (f instanceof MilitaryAirplane) {
                    if (((MilitaryAirplane) f).getEnemy()) {
                        if (Math.abs(f.getPosition() - getPosition()) == sizeY || Math.abs(f.getPosition() - getPosition()) == 1) {
                            //System.out.println("Vozilo je unisteno");
                            Simulator.list.remove(f);
                        }

                    }
                }
        }
    }
    @Override
    public void changeDirection(){

    }

}
