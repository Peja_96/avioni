package project.flyingVehicles;

public class CargoHelicopter extends Helicopter{
    private double maxLoad;
    private Object[] load;
    public CargoHelicopter(int x,int y){
        super("H","brown",x,y);
    }

    public double getMaxLoad() {
        return maxLoad;
    }

    public void setMaxLoad(double maxLoad) {
        this.maxLoad = maxLoad;
    }
}
