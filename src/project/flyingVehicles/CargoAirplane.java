package project.flyingVehicles;

public class CargoAirplane extends Airplane {
    private double maxLoad;
    private Object load;
    public CargoAirplane(int x,int y){
        super("A","brown",x,y);
    }

    public void setMaxLoad(double maxLoad) {
        this.maxLoad = maxLoad;
    }

    public Object getLoad() {
        return load;
    }

    public void setLoad(Object load) {
        this.load = load;
    }

    public double getMaxLoad() {
        return maxLoad;
    }
}
