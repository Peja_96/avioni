package project.flyingVehicles;

public class FireFightingHelicopter extends Helicopter {
    private double maxWaterCapacity;
    public FireFightingHelicopter(int x,int y){
        super("H","red",x,y);
    }
    public void fireFight(){
        System.out.println("Gasenje pozara");
    }
    public double getMaxWaterCapacity() {
        return maxWaterCapacity;
    }

    public void setMaxWaterCapacity(double maxWaterCapacity) {
        this.maxWaterCapacity = maxWaterCapacity;
    }
}
