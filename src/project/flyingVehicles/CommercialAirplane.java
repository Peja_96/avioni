package project.flyingVehicles;

public class CommercialAirplane extends Airplane {
    private int numberOfSeats;
    private int maxLoad;
    public CommercialAirplane(int x, int y){
        super("A","gold",x,y);
    }

    public void setMaxLoad(int maxLoad) {
        this.maxLoad = maxLoad;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public int getMaxLoad() {
        return maxLoad;
    }
}
