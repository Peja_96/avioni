package project.flyingVehicles;

import project.gui.PlatformLook;
import project.persons.Person;
import project.simulation.Simulator;
import project.util.ErrorLogger;
import project.util.SerObject;

import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;

import static project.simulation.Simulator.list;
import static project.simulation.Simulator.sizeX;
import static project.simulation.Simulator.sizeY;

public class FlyingVehicle extends Thread {

    public enum Direction{
        UP,DOWN,RIGHT,LEFT
    }
    private String model;
    private String ID;
    private int height;
    protected int speed;
    private Person[] persons;
    private HashMap<?,?> characteristics;
    private String sign ;
    private String color;
    protected Direction direction;
    protected int coordinateX;
    protected int coordinateY;
    public FlyingVehicle(){}
    public FlyingVehicle(String sign, String color, int x, int y){
        this.sign = sign;
        this.color = color;
        coordinateX = x;
        coordinateY = y;
        setDaemon(true);
        if(x == 0)
            direction = Direction.DOWN;
        else if(x == PlatformLook.sizeX-1)
            direction = Direction.UP;
        else if(y == 0)
            direction = Direction.RIGHT;
        else
            direction = Direction.LEFT;
        speed = new Random().nextInt(3)+1;
        height = 1000;//new Random().nextInt(10)*100+1000;
    }

    public String getColor() {
        return color;
    }

    public int getHeight() {
        return height;
    }

    public HashMap<?, ?> getCharacteristics() {
        return characteristics;
    }

    public int getSpeed() {
        return speed;
    }

    public Person[] getPersons() {
        return persons;
    }

    public String getID() {
        return ID;
    }

    public String getModel() {
        return model;
    }

    public String getSign() {
        return sign;
    }
    public String toString(){
        return getSign();
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setCharacteristics(HashMap<?, ?> characteristics) {
        this.characteristics = characteristics;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setPersons(Person[] persons) {
        this.persons = persons;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void run(){
        while(true){
            if (direction == Direction.DOWN)
                ++coordinateX;
            else if (direction == Direction.UP)
                --coordinateX;
            else if (direction == Direction.RIGHT)
                ++coordinateY;
            else
                --coordinateY;
            if (!(coordinateX < sizeX && coordinateY < sizeY && coordinateX >= 0 && coordinateY >= 0)){
                Simulator.list.remove(this);
                break;
            }
            try {
                if(crashCheck())
                    break;
            }catch (NullPointerException n){
                ErrorLogger.log(Level.INFO,"Vozilo je vec unisteno");
            }

            try {
                sleep(1000 * speed);
            }catch (InterruptedException ex){}
        }
    }
    public void changeDirection(){
        int[] arr = {coordinateY,Simulator.sizeY -coordinateY,coordinateX,Simulator.sizeX - coordinateX};
        int pom = getSmallest(arr);
        switch (pom){
            case 0: {direction = Direction.LEFT;break;}
            case 1: {direction = Direction.RIGHT;break;}
            case 2: {direction = Direction.UP;break;}
            case 3: {direction = Direction.DOWN;break;}

        }
    }
    public boolean crashCheck(){
        for(FlyingVehicle f : Simulator.listCopy())
            if(f != this && f.getPosition() == getPosition() && f.getHeight() == getHeight()){
                new SerObject(f,this).writeInFile();
                list.remove(f);
                list.remove(this);

                return true;
            }
         return false;
    }
    public int getSmallest(int[] arr){
        int small = 0;
        for(int i = 1; i< arr.length ; i++)
            if(arr[i]<arr[small])
                small = i;
            return small;
    }
    public int getPosition(){
        return coordinateX*Simulator.sizeY+coordinateY;
    }
    public String getMapInfo(){
        return toString()+" "+getColor();
    }
    public Direction getDirection(){
        return direction;
    }
    public void setDirection(Direction direction){this.direction = direction;}

 }
