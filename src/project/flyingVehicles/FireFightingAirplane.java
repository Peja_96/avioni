package project.flyingVehicles;

public class FireFightingAirplane extends Airplane{
    private double maxWaterCapacity;
    public FireFightingAirplane(int x,int y){
        super("A","red",x,y);
    }
    public void fireFight(){
        System.out.println("Gasenje pozara");
    }

    public double getMaxWaterCapacity() {
        return maxWaterCapacity;
    }

    public void setMaxWaterCapacity(double maxWaterCapacity) {
        this.maxWaterCapacity = maxWaterCapacity;
    }
}
