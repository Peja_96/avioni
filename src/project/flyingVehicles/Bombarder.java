package project.flyingVehicles;

public class Bombarder extends MilitaryAirplane {
    public Bombarder(int x,int y,boolean e){super("darkgrey",x,y,e);}
    public Bombarder(int x, int y,boolean e,Direction direction){
        super("darkgrey",x,y,e);
        this.direction = direction;
    }
    @Override
    public void attack() {
        super.attack();
    }
}
