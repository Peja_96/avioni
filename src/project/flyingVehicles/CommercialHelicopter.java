package project.flyingVehicles;

public class CommercialHelicopter extends Helicopter {
    private int numberOfSeats;
    public CommercialHelicopter(int x, int y){
        super("H","gold",x,y);
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }
}
