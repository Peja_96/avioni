package project.persons;

public class Person {
    private String name;
    private String surName;
    public Person(){}
    public Person(String name, String surname){
        this.name = name;
        this.surName = surname;
    }

    public String getName() {
        return name;
    }

    public String getSurName() {
        return surName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }
}
