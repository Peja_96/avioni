package project.persons;

public class Pilot extends Person {
    private String license;
    public Pilot(){}
    public Pilot(String name, String surName, String license){
        super(name,surName);
        this.license = license;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }
}
