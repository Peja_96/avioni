package project.persons;

public class Traveler extends Person{
    private String passport;
    public Traveler(){}
    public Traveler(String name, String surName, String passport){
        super(name,surName);
        this.passport = passport;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }
}
