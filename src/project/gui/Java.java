package project.gui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import project.radar.Radar;
import project.simulation.Simulator;
import project.util.ErrorLogger;
import project.util.Zipper;

import java.util.ArrayList;

public class Java extends Application {
    private static BorderPane borderPane;
    public static void main(String[] args) {
        Simulator simulator = new Simulator();
        simulator.startSimulation();
        Radar radar = new Radar();
        radar.start();
        Zipper zip = new Zipper();
        zip.setDaemon(true);
        zip.start();
        ErrorLogger logger = new ErrorLogger("error.log");
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        borderPane = new BorderPane();

        primaryStage.setTitle("Java");
        PlatformLook platformLook = new PlatformLook();
        platformLook.start();
        ScrollPane scrollPane = new ScrollPane(platformLook.getGridPane());
        borderPane.setCenter(scrollPane);
        borderPane.setLeft(new CommandBar().getBar());
        borderPane.setBottom(new Label());
        Scene scene = new Scene(borderPane, 600, 400);
        primaryStage.setOnCloseRequest(event->{
            Platform.exit();
            System.exit(0);
        });
        primaryStage.setScene(scene);
        primaryStage.show();

    }
    public static void setLable(String time,String info){
        Label label = new Label(time + info);
        javafx.application.Platform.runLater(()->{
            borderPane.setBottom(label);
        });
    }
    public static void popup(String str){
        ArrayList<String> list = new ArrayList<>();
        list.add(str);
        javafx.application.Platform.runLater(()->{
            new CrashWindow("Alert",list).show();
        });

    }
}
