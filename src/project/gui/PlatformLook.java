package project.gui;

import javafx.geometry.Insets;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import project.simulation.Simulator;
import project.util.ErrorLogger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;

public class PlatformLook extends Thread {
    private GridPane gridPane;
    public static int sizeX = Simulator.sizeX;
    public static int sizeY = Simulator.sizeY;

    public PlatformLook() {
        gridPane = new GridPane();
        setGridPane();
    }

    private void setGridPane() {
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setPrefSize(800, 800);
        gridPane.addColumn(0);
        gridPane.setStyle("-fx-background-color: #bcfbff;");
        for (int y = 0; y < sizeY; y++) {
            ColumnConstraints cc = new ColumnConstraints();
            cc.setFillWidth(true);
            cc.setHgrow(Priority.ALWAYS);
            cc.setMinWidth(15);
            cc.setMaxWidth(15);
            gridPane.getColumnConstraints().add(cc);
        }

        for (int x = 0; x < sizeX; x++) {
            RowConstraints rc = new RowConstraints();
            rc.setFillHeight(true);
            rc.setVgrow(Priority.ALWAYS);
            rc.setMinHeight(15);
            rc.setMaxHeight(15);
            gridPane.getRowConstraints().add(rc);
        }
        for (int i = 0; i < sizeX; i++)
            for (int j = 0; j < sizeY; j++)
                gridPane.add(new StackPane(), j, i);//zamjenjeno j i i


        gridPane.setGridLinesVisible(true);

    }
    public void run(){
        try{
            sleep(1500);
        }catch (InterruptedException ex){
            ErrorLogger.log(Level.INFO,"Greska prilikom osvjezavanja platforme");        }
        while(true){
            updatePlatform();
            try{
                sleep(2000);
            }catch (InterruptedException ex){
                ErrorLogger.log(Level.INFO,"Greska prilikom osvjezavanja platforme");
            }


        }
    }
    public void updatePlatform(){
        try{
            int pom = -1;
            List<String> list = Files.readAllLines(Paths.get("map.txt"));
            for(String str : list) {
                pom++;
                if (!str.equals("-")) {
                    String[] temp = str.split(" ");
                    addText(temp[0],temp[1], pom / sizeY, pom % sizeY);
                }
                else{
                    rmText(pom/sizeY,pom%sizeY);
                }
            }

        }catch (IOException ioe){
            ErrorLogger.log(Level.INFO,"Greska prilikom ucitavanja mape");
        }
    }

    public StackPane addText(String c,String color, int a, int b) {
        StackPane pane = (StackPane) gridPane.getChildren().get(a * sizeY + b);
        if(pane.getChildren().size()> 0)
            rmText(a,b);
        Text text = new Text(c);
        text.setFill(Color.valueOf(color));
        text.setFont(Font.font(15));
        javafx.application.Platform.runLater(()->{
            pane.getChildren().add(text);
        });
        return pane;
    }


    public void rmText(int a, int b) {
        StackPane pane = (StackPane) gridPane.getChildren().get(a * sizeY + b);
        try {
            javafx.application.Platform.runLater(()->{
                if(pane.getChildren().size() >0){
                    pane.getChildren().remove(0);}
            });
        } catch (IndexOutOfBoundsException ex) {
            ErrorLogger.log(Level.INFO,"Brisanje slova sa mape");
        }
    }

    public GridPane getGridPane() {
        return gridPane;
    }



}

