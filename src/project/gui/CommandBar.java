package project.gui;

import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import project.simulation.Simulator;
import project.util.Loader;


public class CommandBar {
    private VBox vBox;
    public CommandBar(){
        vBox = new VBox();
        setVbox();
    }
    private void setVbox(){
        vBox.setSpacing(40);
        Button activateButton = new Button("Activate");
        activateButton.setPrefSize(100,20);
        activateButton.setOnAction(event -> {
            if(Simulator.isEmpty()) {
               Simulator.normalMode = true;
            }
        });
        Button deActivateButton = new Button("Deactivate");
        deActivateButton.setOnAction(event -> {
            Simulator.changeDirection();
            Simulator.normalMode = false;
        });
        deActivateButton.setPrefSize(100,20);
        Button crashButton = new Button("Events");
        crashButton.setPrefSize(100,20);
        crashButton.setOnAction(event -> {
            new CrashWindow("Events",new Loader().readEvents()).show();
        });
        Button alertButton = new Button("Alerts");
        alertButton.setPrefSize(100,20);
        alertButton.setOnAction(event -> {

            new CrashWindow("Alerts",new Loader().readAlerts()).show();
        });
        vBox.getChildren().addAll(activateButton,deActivateButton,crashButton,alertButton);
    }
    public VBox getBar(){return vBox;}
}
