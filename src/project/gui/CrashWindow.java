package project.gui;

import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import project.simulation.Simulator;
import project.util.Loader;

import java.util.List;

public class CrashWindow {
    private Stage stage;
    private Scene scene;
    private BorderPane borderPane;
    public CrashWindow(String title, List<String> list){
        setBorder(list);
        scene = new Scene(borderPane,600,150);
        stage = new Stage();
        stage.setTitle(title);
        stage.setScene(scene);


    }
    private void setBorder(List<String> list){
        borderPane = new BorderPane();
        VBox vBox = new VBox();
        for(String str : list)
            vBox.getChildren().add(new Text(str));
        ScrollPane scrollPane = new ScrollPane(vBox);
        borderPane.setPrefWidth(vBox.getWidth());
        borderPane.setCenter(scrollPane);
    }
    public void show(){
        stage.show();
    }
}
