package project.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Zipper extends Thread {

    public void run(){
        while (true) {
            try {
                sleep(60000);
            } catch (InterruptedException ie) {

            }
            String pathName = "backup";
            String str = new SimpleDateFormat("yyyy_MM_dd_HH_mm").format(new Date());
            File file = new File(pathName+str+".zip");
            try {
                if(!file.exists())
                    file.createNewFile();
                ZipOutputStream out = new ZipOutputStream(new FileOutputStream(file));
                ZipEntry e = new ZipEntry("map.txt");
                out.putNextEntry(e);
                byte[] data = Files.readAllBytes(Paths.get("map.txt"));
                out.write(data, 0, data.length);
                out.closeEntry();
                File dir = new File("events");
                if(!dir.exists())
                    dir.mkdir();
                String[] list = dir.list();
                for(int i = 0; i < list.length; i++) {
                    try {
                        ZipEntry entry = new ZipEntry(list[i]);
                        out.putNextEntry(entry);
                        byte[] temp = Files.readAllBytes(Paths.get("events"+ File.separator+list[i]));
                        out.write(temp,0,temp.length);
                        out.closeEntry();
                    }catch (IOException io){
                        ErrorLogger.log(Level.INFO,"Greska kod ucitavanja eventsa");
                    }
                }
                out.close();
            }catch (IOException i){
                ErrorLogger.log(Level.INFO,"Greska kod otvaranja fajla");
            }


        }

    }
}
