package project.util;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;

public class Prop {
    Properties properties ;
    FileReader stream;
    public Prop(String fileName)
    {
        try {
            properties = new Properties();
            stream = new FileReader(fileName);
            properties.load(stream);
        } catch (IOException ex) {
            ErrorLogger.log(Level.INFO,"Greska kod propertiesa");
        } finally {
            if (stream != null)
                try {
                    stream.close();
                } catch (IOException ex) {
                    ErrorLogger.log(Level.INFO,"Strim se nije zatvorio");
                }
        }
    }
    public int get(String data)
    {
        try {
            return Integer.parseInt(properties.getProperty(data));
        }catch (NumberFormatException num){
            ErrorLogger.log(Level.INFO,"Greska prilikom parsiranja");
            return 10;
        }

    }
    public boolean getBoolean(String data)
    {
        try {
            return Boolean.parseBoolean(properties.getProperty(data));
        }catch (NumberFormatException num){
            ErrorLogger.log(Level.INFO,"Greska prilikom parsiranja");
            return false;
        }
    }
}
