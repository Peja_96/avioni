package project.util;

import java.io.File;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ErrorLogger {
    public static Logger logger;
    public ErrorLogger(String fileName){
        try{
            File file = new File(fileName);
            if(!file.exists())
                file.createNewFile();

            logger = Logger.getLogger("logger");
            logger.addHandler(new FileHandler(fileName,true));
            logger.setLevel(Level.INFO);
            logger.setUseParentHandlers(false);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    public static void log(Level level, String msg){
        logger.log(level,msg);
    }
}