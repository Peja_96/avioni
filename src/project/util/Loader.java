package project.util;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class Loader {
    public static List<String> readEvents(){
        File file = new File("events");
        if(!file.exists())
            file.mkdir();
        String[] list = file.list();
        List<String> text = new ArrayList<>();
        for(int i = 0; i < list.length; i++) {
            try {
                text.add(Files.readAllLines(Paths.get("events" + File.separator + list[i])).get(0));
            }catch (IOException io){
                ErrorLogger.log(Level.INFO,"Greska kod ucitavanja eventsa");
            }
        }
        return text;
    }
    public static List<String> readAlerts(){
        List<String> alerts = new ArrayList<>();
        File file = new File("alert");
        if(!file.exists())
            file.mkdir();
        String[] list = file.list();
        for(int i = 0; i < list.length; i++) {
            try {
                ObjectInputStream oir = new ObjectInputStream(new FileInputStream(new File("alert" +File.separator+list[i])));
                alerts.add(oir.readObject().toString());
                oir.close();

            }catch (Exception io){
                ErrorLogger.log(Level.INFO,"Greska prilikom deserijalizacije");
            }
        }
        return alerts;
    }
}
