package project.util;

import project.flyingVehicles.FlyingVehicle;
import project.gui.Java;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

import static project.simulation.Simulator.sizeX;
import static project.simulation.Simulator.sizeY;

public class SerObject implements Serializable {
    public String info;
    public String time;
    public SerObject(){}
    public SerObject(FlyingVehicle first, FlyingVehicle second){
        info = "Udes na poziciji [" + first.getPosition()/sizeY + "]["+ first.getPosition()%sizeY + "] izmedju " + first.getClass().getSimpleName()+ " i " + second.getClass().getSimpleName();
        time = new SimpleDateFormat("HH_mm_ss").format(new Date());
        writeInFile();
        Java.popup(info);

    }
    public void writeInFile(){
        try{
            if(!Files.exists(Paths.get("alert")))
                Files.createDirectory(Paths.get("alert"));
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File("alert" + File.separator + time + ".ser")));
            oos.writeObject(this);
            oos.close();
        }catch(Exception ex){
            ErrorLogger.log(Level.INFO,"Greska kod serijalizacije u file");
        }
    }
    public String toString(){
        return time+info;
    }
}
