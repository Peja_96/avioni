package project.rockets;

import java.util.Random;

public  class Rocket extends Thread {
    private double range;
    private double height;
    private int speed;
    private int coordinateX;
    private int coordinateY;
    enum TypeOfRocket{
        MILITARY, WEATHER
    }
    TypeOfRocket typeOfRocket;
    public Rocket(int x,int y){
        Random rand = new Random();
        coordinateX = x;
        coordinateY = y;
        typeOfRocket = TypeOfRocket.values()[rand.nextInt(2)];
        height = rand.nextInt(10)*100+1000;
        speed = rand.nextInt(3)+1;
    }
    public void run(){
        while(true){
            try{
                sleep(speed*1000);
            }catch (InterruptedException ex){

            }
            if((height+=100)>5000)
                break;
        }
    }

}
