package project.radar;

import project.flyingVehicles.FlyingVehicle;
import project.flyingVehicles.MilitaryAirplane;
import project.gui.Java;
import project.simulation.Simulator;
import project.util.ErrorLogger;
import project.util.Prop;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;


public  class Radar extends Thread{
    private int pom = 0;
    private int controlTime;
    public Radar(){
        setDaemon(true);
        controlTime = new Prop("prop" + File.separator + "radar.properties").get("Control time");
    }
    public void run(){

        while(true){

            try{
                sleep(controlTime * 1000);
            }catch (InterruptedException ex){
                ErrorLogger.log(Level.INFO,"Greska prilikom pauziranja treda");
            }
            writeInFile();
            checkForEnemy();
        }
    }
    public static void writeInFile(){

        try {
            Files.write(Paths.get("map.txt"),Simulator.toList2(), Charset.defaultCharset());
        }catch (Exception e){
            e.printStackTrace();
            ErrorLogger.log(Level.INFO,"Greska prilikom upisivanja u map.txt");
        }
    }
    public void checkForEnemy(){
        for(FlyingVehicle f : Simulator.listCopy())
            if(f instanceof MilitaryAirplane) {
                if(((MilitaryAirplane) f).getEnemy() && pom++ == 0){
                    createFile(f);
                    Simulator.changeDirection();
                    Simulator.normalMode = false;
                    Simulator.chaseEnemy(f.getPosition(), f.getDirection());
                    return;
                }
                else if(((MilitaryAirplane)f).getEnemy())
                    return;
            }
            //Simulator.normalMode = true;
    }

    public void createFile(FlyingVehicle plane){

        try {
            if(!Files.exists(Paths.get("events")))
                Files.createDirectory(Paths.get("events"));
            String str = new SimpleDateFormat("HH_mm_ss").format(new Date());
            Path path = Paths.get("events"+ File.separator+str +".txt");
            Files.createFile(path);
            String text = "Strano vozilo je uoceno na poziciji [" + plane.getPosition()/Simulator.sizeY + "][" + plane.getPosition()%Simulator.sizeY+"]";
            Java.setLable(str,text);
            Files.write(path,text.getBytes());
        }catch (IOException i){
            ErrorLogger.log(Level.INFO,"Greska prilikom uocavanja stranog vozila");
        }




    }
}
