package project.simulation;

import project.flyingVehicles.*;
import project.util.Prop;


import java.io.File;
import java.util.*;
import java.util.List;

public class Simulator extends Thread{
    public static boolean normalMode;
    public static int sizeX ;
    public static int sizeY ;
    private int creationTime;
    public static List<FlyingVehicle> list = new ArrayList<>();
    private Watch watch;
    public Simulator(){
        Prop prop = new Prop("prop" + File.separator +"config.properties");
        sizeX = prop.get("DimensionX");
        sizeY = prop.get("DimensionY");
        creationTime = prop.get("Time of creation");
        setDaemon(true);
        watch = new Watch();
        watch.setDaemon(true);
        watch.start();
        normalMode = true;
    }
    public void run(){

        while(true){
            if(normalMode)
            generateVehicle();
            try {
                sleep(creationTime*1000);
            }catch (InterruptedException ex){

            }

        }
    }
    public void generateVehicle() {
        Random rand = new Random();
        while (true) {
            int x, y;
            switch (rand.nextInt(4)) {
                case 0: {
                    x = 0;
                    y = rand.nextInt(sizeY);
                    break;
                }
                case 1: {
                    y = 0;
                    x = rand.nextInt(sizeX);
                    break;
                }
                case 2: {
                    y = sizeY - 1;
                    x = rand.nextInt(sizeX);
                    break;
                }
                default: {
                    x = sizeX - 1;
                    y = rand.nextInt(sizeY);
                }
            }
            switch (rand.nextInt(6)) {
                case 0: {
                    list.add(new CommercialAirplane(x, y));
                    break;
                }
                case 1: {
                    list.add(new CargoAirplane(x, y));
                    break;
                }
                case 2: {
                    list.add(new FireFightingHelicopter(x, y));
                    break;
                }
                case 3: {
                    list.add(new FireFightingAirplane(x, y));
                    break;
                }
                case 4: {
                    list.add(new CommercialHelicopter(x, y));
                    break;
                }
                case 5: {
                    list.add(new CargoHelicopter(x, y));
                    break;
                }

            }
            (list.get(list.size() - 1)).start();
            break;
        }
    }

    public static void generateMilitary(boolean enemy){
        Random rand = new Random();

            int x,y;
            switch(rand.nextInt(4)){
                case 0: {x = 0;y = rand.nextInt(sizeY); break;}
                case 1: {y = 0;x = rand.nextInt(sizeX);break;}
                case 2: {y = sizeY -1;x = rand.nextInt(sizeX);break;}
                default : {x = sizeX -1;y = rand.nextInt(sizeY);}
            }
            switch (rand.nextInt(2)){
                case 0:{list.add(new Fighter(x,y,enemy));break;}
                default: {list.add(new Bombarder(x,y,enemy));break;}
            }
            (list.get(list.size()-1)).start();

    }
    public static List<String> toList2(){
        List<String> lista = new ArrayList<>();
        List<FlyingVehicle> temp = new ArrayList<>(list);
        for(int i = 0; i< sizeY*sizeX; i++)
        lista.add("-");
        for(FlyingVehicle f : temp) {
            lista.set(f.getPosition(),f.getMapInfo());
        }
        return lista;
    }
    public static boolean isEmpty(){
        for(String s : toList2())
            if(!s.equals("-"))
                return false;
            return true;

        }
    public void startSimulation(){
        this.start();
    }
    public static void changeDirection(){
        for (FlyingVehicle f:list)
            f.changeDirection();
    }
    public static void chaseEnemy(int position, FlyingVehicle.Direction direction){
        try{
            sleep(1000);
        }catch (InterruptedException i ){}
        if(direction == FlyingVehicle.Direction.LEFT) {
            list.add(new Fighter(setCoordinateX(position,true), sizeY - 1, false,direction));
            list.add(new Bombarder(setCoordinateX(position,false), sizeY - 1, false,direction));
        }
        else if(direction == FlyingVehicle.Direction.RIGHT) {
            list.add(new Fighter(setCoordinateX(position, true), 0, false,direction));
            list.add(new Bombarder(setCoordinateX(position, false), 0, false,direction));
        }
        else if(direction == FlyingVehicle.Direction.UP) {
            list.add(new Fighter(sizeX - 1, setCoordinateY(position, true), false,direction));
            list.add(new Bombarder(sizeX - 1, setCoordinateY(position, false), false,direction));
        }
        else if(direction == FlyingVehicle.Direction.DOWN) {
            list.add(new Fighter(0, setCoordinateY(position,true), false,direction));
            list.add(new Bombarder(0, setCoordinateY(position,false), false,direction));
        }
        list.get(list.size()-1).start();
        list.get(list.size()-2).start();
    }
    public static List<FlyingVehicle> listCopy(){
        return new ArrayList<>(list);
    }
    public static int setCoordinateX(int a,boolean less){//mjenjane neke stvari
        if(less){
            int pom = a/sizeY-1;
            if(pom >=0)
                return pom;
            else return pom+1;
        }
        else {
            int pom = a/sizeY+1;
            if(pom >sizeY-1)
                return pom -1;
            else return pom;
        }
    }public static int setCoordinateY(int a,boolean less){
        if(less){
            int pom = a%sizeY-1;
            if(pom >=0)
                return pom;
            else return pom+1;
        }
        else {
            int pom = a%sizeY+1;
            if(pom >sizeY-1)
                return pom-1;
            else return pom;
        }
    }

}
