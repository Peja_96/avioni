package project.simulation;

import project.util.ErrorLogger;
import project.util.Prop;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.logging.Level;

public class Watch extends Thread{
    private Prop prop;
    private static int count1 = 0;
    private static int count2 = 0;
    public void run(){
        try(WatchService ws = FileSystems.getDefault().newWatchService()){
            Path path = Paths.get("prop");
            path.register(ws, StandardWatchEventKinds.ENTRY_MODIFY);
            WatchKey key;
            do{
                key = ws.take();
                try{
                    sleep(100);
                }catch (InterruptedException e){}

                if(key.pollEvents().size()>0) {

                    prop = new Prop("prop"+ File.separator +"config.properties");
                    if(prop.getBoolean("Enemy") && count1++==0)
                        Simulator.generateMilitary(true);
                    if(prop.getBoolean("Native") && count2++ == 0)
                        Simulator.generateMilitary(false);
                }
            }while(key.reset());
        }
        catch (IOException e){
            ErrorLogger.log(Level.INFO,"Greska kod watchera");
        }
        catch (InterruptedException a){
            ErrorLogger.log(Level.INFO,"Greska prilikom otvaranja prop");
        }
    }

}